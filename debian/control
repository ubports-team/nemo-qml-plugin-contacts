Source: nemo-qml-plugin-contacts
Section: libs
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>,
           Guido Berhörster <guido+debian@berhoerster.name>,
Build-Depends: debhelper-compat (= 13),
               pkgconf,
               qtcontacts5-plugin-sqlite-dev,
               libaccounts-qt5-dev,
               libphonenumber-dev,
               libmlocale-dev,
               qtdeclarative5-dev,
               qtbase5-dev,
               qtpim5-dev,
               qtpim5-private-dev,
               libgsettings-qt-dev,
               qttools5-dev,
               qttools5-dev-tools
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/sailfishos/nemo-qml-plugin-contacts
Vcs-Git: https://salsa.debian.org/ubports-team/nemo-qml-module-contacts.git
Vcs-Browser: https://salsa.debian.org/ubports-team/nemo-qml-module-contacts

Package: qml-module-org-nemomobile-contacts
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libqt5contact5-plugin-sqlite,
         ${misc:Depends},
         ${shlibs:Depends},
Description: QML module providing access to QtPIM QtContacts
 This QML module provides high-level abstraction layer on top of the QtPIM
 QtContacts API with some improvements such as a data cache, localisation and
 internationalization enablers, and other utility capabilities.

Package: libcontactcache-qt5-1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libqt5contact5-plugin-sqlite,
         libcontactcache-qt5-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Library for accessing QtPIM QtContacts using the SQLite engine
 This library acts as a middle layer between the QtPIM QtContacts SQLite engine
 and client components and provides a data cache.

Package: libcontactcache-qt5-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: qtpim5-dev,
         qtcontacts5-plugin-sqlite-dev,
         libcontactcache-qt5-1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Development files for libcontactcache-qt5
 This library acts as a middle layer between the QtPIM QtContacts SQLite engine
 and client components and provides a data cache.
 .
 This package provides the development files.

Package: libcontactcache-qt5-data
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends},
Description: Library for accessing QtPIM QtContacts using the SQLite engine - data
 This library acts as a middle layer between the QtPIM QtContacts SQLite engine
 and client components and provides a data cache.
 .
 This package provides data files used by the shared library.

Package: libcontactcache-qt5-bin
Section: devel
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: libqt5contact5-plugin-sqlite,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Development tools for libcontactcache-qt5
 This library acts as a middle layer between the QtPIM QtContacts SQLite engine
 and client components and provides a data cache.
 .
 This package provides development tools.
